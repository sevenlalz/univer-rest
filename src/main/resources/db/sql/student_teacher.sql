CREATE TABLE student_teacher
(
    student_id int not null,
    teacher_id int not null,
    primary key (student_id, teacher_id),
    foreign key (student_id) references students(student_id),
    foreign key (teacher_id) references teachers(teacher_id)
)