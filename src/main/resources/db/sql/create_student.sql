CREATE TABLE students
(
    id     int         NOT NULL PRIMARY KEY,
    name   varchar(25) NOT NULL,
    course int         NOT NULL
)