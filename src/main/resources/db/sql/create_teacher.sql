CREATE TABLE teachers
(
    id         int         NOT NULL PRIMARY KEY,
    name       varchar(25) NOT NULL,
    work_start date        NOT NULL
)