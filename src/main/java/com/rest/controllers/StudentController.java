package com.rest.controllers;

import com.rest.data.entity.Student;
import com.rest.data.service.StudentRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    private final StudentRepoService studentRepoService;

    @Autowired
    public StudentController(@Qualifier("studentRepoServiceImpl") StudentRepoService studentRepoService) {
        this.studentRepoService = studentRepoService;
    }

    @PostMapping("/")
    public void addStudent(@RequestBody Student student) {
        studentRepoService.addStudent(student);
    }

    @GetMapping("/")
    public List<Student> getAll() {
        return studentRepoService.getAll();
    }

    @GetMapping("/{id}")
    public Student get(@PathVariable(name = "id") int id) {
        return studentRepoService.getOneById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") int id) {
        studentRepoService.deleteStudent(id);
    }

    @DeleteMapping("/")
    public void delete(@RequestBody Student student) {
        studentRepoService.deleteStudent(student);
    }
}
