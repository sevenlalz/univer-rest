package com.rest.controllers;

import com.rest.data.entity.Student;
import com.rest.data.entity.Teacher;
import com.rest.data.service.TeacherRepoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherRepoService teacherRepoService;

    @Autowired
    public TeacherController(TeacherRepoService teacherRepoService) {
        this.teacherRepoService = teacherRepoService;
    }

    @PostMapping("/")
    public void addTeacher(@RequestBody Teacher teacher) {
        teacherRepoService.addTeacher(teacher);
    }

    @GetMapping("/")
    public List<Teacher> getAll() {
        return teacherRepoService.getAll();
    }

    @GetMapping("/{id}")
    public Teacher get(@PathVariable(name = "id") int id) {
        return teacherRepoService.getOneById(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") int id) {
        teacherRepoService.deleteTeacher(id);
    }

    @DeleteMapping("/")
    public void delete(@RequestBody Teacher teacher) {
        teacherRepoService.deleteTeacher(teacher);
    }
}
