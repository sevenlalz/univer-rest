package com.rest.controllers;

import com.rest.data.entity.Student;
import com.rest.data.service.StudentTeacherLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/student-teacher-link")
public class StudentTeacherLinkController {

    private final StudentTeacherLink studentTeacherLink;

    @Autowired
    public StudentTeacherLinkController(@Qualifier("studentTeacherLinkImpl") StudentTeacherLink studentTeacherLink) {
        this.studentTeacherLink = studentTeacherLink;
    }

    @PutMapping("/{id}")
    public void addStudentToTeacher(@RequestBody Student student,
                                    @PathVariable(name = "id") int teacherId) {
        studentTeacherLink.addStudentToTeacher(student, teacherId);
    }


    @DeleteMapping("/{student_id}/{teacher_id}")
    public void deleteStudentFromTeacher(@PathVariable(name = "student_id") int studentId,
                                         @PathVariable(name = "teacher_id") int teacherId) {
        studentTeacherLink.deleteStudentFromTeacher(studentId, teacherId);
    }
}
