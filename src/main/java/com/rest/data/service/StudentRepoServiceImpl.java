package com.rest.data.service;

import com.rest.data.dao.StudentRepo;
import com.rest.data.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class StudentRepoServiceImpl implements StudentRepoService {
    private final StudentRepo studentRepo;
    private final EntityManager manager;

    @Autowired
    public StudentRepoServiceImpl(@Qualifier("studentRepo") StudentRepo studentRepo,
                                  @Qualifier("entityManagerFactory") EntityManager manager) {
        this.studentRepo = studentRepo;
        this.manager = manager;
    }

    @Override
    public void addStudent(Student student) {
        studentRepo.save(student);
    }

    @Override
    public void deleteStudent(Student student) {
        studentRepo.delete(student);
    }

    @Override
    public Student getOneById(int id) {
        //Session session = manager.unwrap(Session.class); //получаем сессию для хайбернейт. Не особо важная вещь
        Query query = manager.createQuery("select s from Student s where s.student_id=:studentId");
        query.setParameter("studentId", id);
        return (Student) query.getSingleResult();
    }

    @Override
    public List<Student> getAll() {
        return studentRepo.findAll();
    }

    @Override
    public void deleteStudent(int id) {
        studentRepo.deleteById(id);
    }
}
