package com.rest.data.service;

import com.rest.data.dao.TeacherRepo;
import com.rest.data.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Service
public class TeacherRepoServiceImpl implements TeacherRepoService {

    private final TeacherRepo teacherRepo;
    private final EntityManager manager;

    @Autowired
    public TeacherRepoServiceImpl(@Qualifier("teacherRepo") TeacherRepo teacherRepo,
                                  @Qualifier("entityManagerFactory") EntityManager manager) {
        this.teacherRepo = teacherRepo;
        this.manager = manager;
    }

    @Override
    public void addTeacher(Teacher teacher) {
        teacherRepo.save(teacher);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        teacherRepo.delete(teacher);
    }

    @Override
    public Teacher getOneById(int id) {
        //        Session session = manager.unwrap(Session.class); //получаем сессию для хайбернейт. Не особо важная вещь
        Query query = manager.createQuery("select t from Teacher t where t.teacher_id=:teacherId");
        query.setParameter("teacherId", id);
        return (Teacher) query.getSingleResult();
    }

    @Override
    public List<Teacher> getAll() {
        return teacherRepo.findAll();
    }

    @Override
    public void deleteTeacher(int id) {
        teacherRepo.deleteById(id);
    }
}
