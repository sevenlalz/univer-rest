package com.rest.data.service;

import com.rest.data.dao.StudentRepo;
import com.rest.data.dao.TeacherRepo;
import com.rest.data.entity.Student;
import com.rest.data.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Service
public class StudentTeacherLinkImpl implements StudentTeacherLink {

    private final EntityManager manager;
    private final StudentRepo studentRepo;
    private final TeacherRepo teacherRepo;

    @Autowired
    public StudentTeacherLinkImpl(@Qualifier("entityManagerFactory") EntityManager manager,
                                  @Qualifier("studentRepo") StudentRepo studentRepo,
                                  @Qualifier("teacherRepo") TeacherRepo teacherRepo) {
        this.manager = manager;
        this.studentRepo = studentRepo;
        this.teacherRepo = teacherRepo;
    }

    @Override
    public void addStudentToTeacher(Student student, int teacherId) {
        Teacher teacher = teacherRepo.getOne(teacherId);
        studentRepo.save(student);
        teacher.addStudent(student);
        teacherRepo.save(teacher);
    }

    @Override
    public void deleteStudentFromTeacher(int studentId, int teacherId) {
        Student student = studentRepo.getOne(studentId);
        Teacher teacher = teacherRepo.getOne(teacherId);
        teacher.deleteStudent(student);
        teacherRepo.save(teacher);
    }

    @Override
    public List<Student> getStudentListByTeacher(Teacher teacher) {
        Optional<Teacher> opt = teacherRepo.findById(teacher.getTeacher_id());
        return opt.orElseThrow().getStudents();
    }

    @Override
    public List<Student> getStudentListByTeacher(int id) {
        Optional<Teacher> opt = teacherRepo.findById(id);
        return opt.orElseThrow().getStudents();
    }

    @Override
    public List<Teacher> getTeacherListByStudent(Student student) {
        return null;
    }

    @Override
    public List<Teacher> getTeacherListByStudent(int id) {
        return null;
    }
}
