package com.rest.data.service;

import com.rest.data.entity.Teacher;

import java.util.List;

/**
 * 2 task
 */
public interface TeacherRepoService {

    void addTeacher(Teacher teacher);

    void deleteTeacher(Teacher teacher);

    Teacher getOneById(int id);

    List<Teacher> getAll();

    void deleteTeacher(int id);
}
