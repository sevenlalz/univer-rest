package com.rest.data.service;

import com.rest.data.entity.Student;

import java.util.List;

/**
 * 1 task
 */
public interface StudentRepoService {

    void addStudent(Student student);

    void deleteStudent(Student student);

    Student getOneById(int id);

    List<Student> getAll();

    void deleteStudent(int id);
}
