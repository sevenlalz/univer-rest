package com.rest.data.service;

import com.rest.data.entity.Student;
import com.rest.data.entity.Teacher;

import java.util.List;

public interface StudentTeacherLink {
    void addStudentToTeacher(Student student, int teacherId);

    void deleteStudentFromTeacher(int studentId, int teacherId);

    List<Student> getStudentListByTeacher(Teacher teacher);

    List<Student> getStudentListByTeacher(int id);

    //overload
    List<Teacher> getTeacherListByStudent(Student student);

    //overload
    List<Teacher> getTeacherListByStudent(int id);
}
