package com.rest.data.entity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.Data;

import javax.persistence.*;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Data
@Entity
@Table(name = "teachers")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int teacher_id;

    @Column(name = "name")
    private String name;

    @Column(name = "work_start")
    private Date date;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "student_teacher",
            joinColumns = @JoinColumn(name = "teacher_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    @JsonSerialize(using = Student.CustomStudentSerializer.class)
    private List<Student> students;

    public void addStudent(Student student) {
        students = Objects.requireNonNullElseGet(students, ArrayList::new);
        students.add(student);
    }

    public void  deleteStudent(Student student) {
        Objects.requireNonNull(students).remove(student);
    }


    public static class CustomTeacherSerializer extends StdSerializer<List<Student>> {
        protected CustomTeacherSerializer(Class<List<Student>> s) {
            super(s);
        }

        protected CustomTeacherSerializer() {
            this(null);
        }

        @Override
        public void serialize(List<Student> value,
                              JsonGenerator gen,
                              SerializerProvider provider) throws IOException {
            List<Student> students = new ArrayList<>();
            for (Student s : value) {
                System.out.println(s.getName());
                s.setTeachers(null);
                students.add(s);
            }
            gen.writeObject(students);
        }
    }
}
