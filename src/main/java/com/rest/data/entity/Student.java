package com.rest.data.entity;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import lombok.Data;

import javax.persistence.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int student_id;

    @Column(name = "name")
    private String name;

    @Column(name = "course")
    private int course;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "student_teacher",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    @JsonSerialize(using = Student.CustomStudentSerializer.class)
    private List<Teacher> teachers;

    public void addTeacher(Teacher teacher) {
        teachers = Objects.requireNonNullElseGet(teachers, ArrayList::new);
        teachers.add(teacher);
    }

    public void deleteTeacher(Teacher teacher) {
        Objects.requireNonNull(teachers).remove(teacher);
    }


    public static class CustomStudentSerializer extends StdSerializer<List<Teacher>> {
        protected CustomStudentSerializer(Class<List<Teacher>> t) {
            super(t);
        }

        protected CustomStudentSerializer() {
            this(null);
        }

        @Override
        public void serialize(List<Teacher> value,
                              JsonGenerator gen,
                              SerializerProvider provider) throws IOException {
            List<Teacher> teachers = new ArrayList<>();
            for (Teacher t : value) {
                System.out.println(t.getName());
                t.setStudents(null);
                teachers.add(t);
            }
            gen.writeObject(teachers);
        }
    }
}

