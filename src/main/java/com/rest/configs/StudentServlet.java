package com.rest.configs;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class StudentServlet extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{LocalStudentServletConfig.class}; //этот конфиг нас не интересует
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/student"};
    }
}
