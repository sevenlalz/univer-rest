//package com.rest.configs;
//
//import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//
//public class TeacherServlet extends AbstractAnnotationConfigDispatcherServletInitializer {
//    @Override
//    protected Class<?>[] getRootConfigClasses() {
//        return new Class[0]; //этот конфиг нас не интересует, т.к. нет рутового конфига
//    }
//
//    @Override
//    protected Class<?>[] getServletConfigClasses() {
//        return new Class[0]; //этот тоже
//    }
//
//    @Override
//    protected String[] getServletMappings() {
//        return new String[]{"/student"};
//    }
//}
